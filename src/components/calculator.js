import { Component } from "react";
import { SoButton, ExtendedSoButton } from "./banPhimButton";

class Calculator extends Component {
    render() {
        return (
            <div style={{ backgroundColor: "#3b4654", color: "white", display: "inline-block", margin: "2rem", padding: "1px" }}>
                <div style={{ padding: "1rem", textAlign: "end" }}>
                    <div style={{ color: "#909092" }}>2536 + 419 +</div>
                    <div style={{ height: "1px", width: "100%", borderTop: "1px dashed #909092", marginTop: "5px", marginBottom: "8px" }}></div>
                    <div style={{ fontSize: "2rem" }}>2955</div>
                </div>
                <div className="banPhim">
                    <div>
                        <ExtendedSoButton style={{ fontWeight: "bold" }} txColor="#bc615c" bgColor="#404d5e">C</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">#</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">%</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">/</ExtendedSoButton>
                    </div>
                    <div>
                        <ExtendedSoButton>7</ExtendedSoButton>
                        <ExtendedSoButton>8</ExtendedSoButton>
                        <ExtendedSoButton>9</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">x</ExtendedSoButton>
                    </div>
                    <div>
                        <ExtendedSoButton>4</ExtendedSoButton>
                        <ExtendedSoButton>5</ExtendedSoButton>
                        <ExtendedSoButton>6</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">-</ExtendedSoButton>
                    </div>
                    <div>
                        <ExtendedSoButton>1</ExtendedSoButton>
                        <ExtendedSoButton>2</ExtendedSoButton>
                        <ExtendedSoButton>3</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">+</ExtendedSoButton>
                    </div>
                    <div>
                        <ExtendedSoButton>.</ExtendedSoButton>
                        <ExtendedSoButton>0</ExtendedSoButton>
                        <ExtendedSoButton>&lt;</ExtendedSoButton>
                        <ExtendedSoButton txColor="#9fa7ac" bgColor="#404d5e">=</ExtendedSoButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default Calculator;