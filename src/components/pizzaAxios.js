import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";

class PizzaAxios extends Component {
    axiosCall = async (url, body) => {
        const response = await axios(url, body);

        return response.data;
    }

    onBtnGetAllOrderClick = () => {
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onCreateOrderClick = () => {
        const body = {
            method: 'POST',
            data: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Bình",
                thanhTien: "200000",
                email: "binhpt001@gmail.com",
                soDienThoai: "0865247777",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders", body)
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onGetOrderByIdClick = () => {
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders/otjiLHAcd1")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onUpdateOrderClick = () => {
        const body = {
            method: 'PUT',
            data: JSON.stringify({
                trangThai: "open" 
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/orders/190734", body)
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onCheckVoucherIdClick = () => {
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/24864")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    onGetDrinkListClick = () => {
        this.axiosCall("http://203.171.20.210:8080/devcamp-pizza365/drinks")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error.message);
        })
    }

    render() {
        return (
            <div className="container p-3" style={{ backgroundColor: "#f8f7f7" }}>
                <p className="mt-4">Test Page for Javascrip Tasks. F5 to run code. </p>
                <div>
                    <button className="btn btn-info m-1" onClick={this.onBtnGetAllOrderClick}>Call api get all orders!</button>
                    <button className="btn btn-success m-1" onClick={this.onCreateOrderClick}>Call api create order!</button>
                    <button className="btn btn-warning m-1" onClick={this.onGetOrderByIdClick}>Call api get order by id!</button>
                    <button className="btn btn-primary m-1" onClick={this.onUpdateOrderClick}>Call api update order!</button>
                    <button className="btn btn-info m-1" onClick={this.onCheckVoucherIdClick}>Call api check voucher by id!</button>
                    <button className="btn btn-danger m-1" onClick={this.onGetDrinkListClick}>Call api Get drink list!</button>
                </div>
                <div>
                    <p className="h4 mt-4"> Demo 06 API for Pizza 365 Project: </p>
                </div>
                <div>
                    <ul>
                        <li>get all Orders: lấy tất cả orders </li>
                        <li>create Order: tạo 1 order</li>
                        <li>get Order by id: lấy 1 order bằng id </li>
                        <li>update Order: update 01 order</li>
                        <li>check voucher by id: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                        <li>get drink list: lấy danh sách đồ uống</li>
                    </ul>
                </div>
                <div>
                    <strong className="text-danger"> Bật console log để nhìn rõ output </strong>
                </div>
            </div>
        );
    }
}

export default PizzaAxios;