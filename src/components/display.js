import { Component } from "react";

class Display extends Component {
    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
    }

    componentWillUnmount() {
        console.log('Component Will Unmount');
    }

    render() {
        return (
            <>
                <h1>I exist !</h1>
            </>
        );
    }
}

export default Display;