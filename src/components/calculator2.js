import { Component } from "react";
import { PropsButton } from "./banPhimButton";

class Calculator2 extends Component {
    render() {
        return (
            <div style={{ display: "inline-block", margin: "2rem", padding: "5px", border: "5px solid #457c7b" }}>
                <div style={{ height: "120px", backgroundImage: "linear-gradient(to right, #efeef0 , #cfd3d8)" }}></div>
                <div style={{ marginTop: "5px" }}>
                    <div style={{ display: "flex" }}>
                        <PropsButton btnWidth="132px" backColor= "#cfd3d8" fontColor="gray">DEL</PropsButton>
                        <PropsButton backColor= "#cfd3d8" fontColor="gray">&#129056;</PropsButton>
                        <PropsButton backColor= "#b8d6d5" fontColor="gray">/</PropsButton>
                    </div>
                    <div>
                        <PropsButton>7</PropsButton>
                        <PropsButton>8</PropsButton>
                        <PropsButton>9</PropsButton>
                        <PropsButton backColor= "#b8d6d5" fontColor="gray">*</PropsButton>
                    </div>
                    <div>
                        <PropsButton>4</PropsButton>
                        <PropsButton>5</PropsButton>
                        <PropsButton>6</PropsButton>
                        <PropsButton backColor= "#b8d6d5" fontColor="gray">-</PropsButton>
                    </div>
                    <div>
                        <PropsButton>1</PropsButton>
                        <PropsButton>2</PropsButton>
                        <PropsButton>3</PropsButton>
                        <PropsButton backColor= "#b8d6d5" fontColor="gray">+</PropsButton>
                    </div>
                    <div style={{ display: "flex" }}>
                        <PropsButton>0</PropsButton>
                        <PropsButton>,</PropsButton>
                        <PropsButton btnWidth="132px" fontColor="white" style={{backgroundImage: "linear-gradient(to right, #519492 , #90cbc0)"}}>=</PropsButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default Calculator2;