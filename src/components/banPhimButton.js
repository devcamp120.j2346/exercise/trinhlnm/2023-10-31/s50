import styled from "styled-components"

const SoButton = styled.button`
border: none;
padding: 1rem;
margin: 1px;
width: 3rem;
height: 3.2rem;
`

const ExtendedSoButton = styled(SoButton)`
background-color: ${props => props.bgColor || "#425062"};
color:  ${props => props.txColor || "white"};
`

const PropsButton = styled.button`
background-color: ${props => props.backColor || "#e4e5ea"};
color:  ${props => props.fontColor || "#80acae"};
border: none;
width: ${props => props.btnWidth || "65px"};
height: 55px;
margin: 1px;
font-weight: bold;
`

export {SoButton, ExtendedSoButton, PropsButton}
