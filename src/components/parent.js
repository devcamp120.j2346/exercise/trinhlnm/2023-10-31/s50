import { Component } from "react";
import Display from "./display";

class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: false
        }
    }

    onCreateButtonClicked = () => {
        this.setState({
            display: true
        });
    }

    onDestroyButtonClicked = () => {
        this.setState({
            display: false
        });
    }

    render() {
        return (
            <div style={{margin: "2rem"}}>
                <div>
                    {this.state.display 
                        ? <button onClick={this.onDestroyButtonClicked} >Destroy Component</button>
                        : <button onClick={this.onCreateButtonClicked}>Create Component</button>
                    }
                </div>
                <div>{this.state.display ? <Display display={this.state.display}/> : null}</div>
            </div>
        );
    }
}

export default Parent;