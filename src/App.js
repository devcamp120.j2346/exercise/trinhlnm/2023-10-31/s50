import Calculator from "./components/calculator";
import Calculator2 from "./components/calculator2";
import Parent from "./components/parent";
import PizzaAxios from "./components/pizzaAxios";
import PizzaFetch from "./components/pizzaFetch";

function App() {
  return (
    <div>
      {/* <Parent/> */}

      {/* <PizzaFetch/> */}

      {/* <PizzaAxios/> */}

      {/* <Calculator/> */}

      <Calculator2/>
    </div>
  );
}

export default App;
